/**
 * @file tests/json.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/utils/JsonReader.hpp>

#define BOOST_TEST_MODULE Utils_Json_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

struct InputData {
  void parse(const nlohmann::json & /* data */) {}
};

struct OutputData {
  void parse(const nlohmann::json & /* data */) {}
};

struct NodeData {
  void parse(const nlohmann::json & /* data */) {}
};

struct LinkData {
  void parse(const nlohmann::json & /* data */) {}
};

BOOST_AUTO_TEST_CASE(TestCase_Json_1)
{
  std::string str = R"({"level":"meso","inputs":[],"outputs":[],"nodes":[{"ID":1,"x":0,"y":0},{"ID":2,"x":0,"y":0},{"ID":3,"x":0,"y":0}],"links":[{"ID":1,"origin":1,"destination":2,"length":500,"max speed":30,"lane number":1}]})";
  artis::traffic::utils::JsonReader<InputData, OutputData, NodeData, LinkData> reader;

  reader.parse_network(str);

  const artis::traffic::utils::Network<InputData, OutputData, NodeData, LinkData> &network = reader.network();

  BOOST_CHECK_EQUAL(network.nodes.size(), 3);
  BOOST_CHECK_EQUAL(network.nodes.at(1).ID, 1);
  BOOST_CHECK_EQUAL(network.nodes.at(1).level, artis::traffic::utils::MESO);
  BOOST_CHECK_EQUAL(network.nodes.at(1).coordinates.x, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(1).coordinates.y, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(2).ID, 2);
  BOOST_CHECK_EQUAL(network.nodes.at(2).level, artis::traffic::utils::MESO);
  BOOST_CHECK_EQUAL(network.nodes.at(2).coordinates.x, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(2).coordinates.y, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(3).ID, 3);
  BOOST_CHECK_EQUAL(network.nodes.at(3).level, artis::traffic::utils::MESO);
  BOOST_CHECK_EQUAL(network.nodes.at(3).coordinates.x, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(3).coordinates.y, 0);
  BOOST_CHECK_EQUAL(network.links.size(), 1);
  BOOST_CHECK_EQUAL(network.links.at(1).origin_nodeID, 1);
  BOOST_CHECK_EQUAL(network.links.at(1).destination_nodeID, 2);
  BOOST_CHECK_EQUAL(network.links.at(1).length, 500);
  BOOST_CHECK_EQUAL(network.links.at(1).max_speed, 30);
  BOOST_CHECK_EQUAL(network.links.at(1).lane_number, 1);
}

BOOST_AUTO_TEST_CASE(TestCase_Json_2)
{
  std::string str = R"({"level":"meso","inputs":[],"outputs":[],"nodes":[{"ID":1,"x":0,"y":0},{"ID":2,"x":500,"y":0}],"links":[{"ID":1,"origin":1,"destination":2,"max speed":30}]})";
  artis::traffic::utils::JsonReader<InputData, OutputData, NodeData, LinkData> reader;

  reader.parse_network(str);

  const artis::traffic::utils::Network<InputData, OutputData, NodeData, LinkData> &network = reader.network();

  BOOST_CHECK_EQUAL(network.links.at(1).length, 500);
  BOOST_CHECK_EQUAL(network.links.at(1).lane_number, 1);
}

BOOST_AUTO_TEST_CASE(TestCase_Json_3)
{
  std::string str = R"({"level":"meso","inputs":[],"outputs":[],"nodes":[{"ID":1},{"ID":2}],"links":[{"ID":1,"origin":1,"destination":2,"length":500,"max speed":30}]})";
  artis::traffic::utils::JsonReader<InputData, OutputData, NodeData, LinkData> reader;

  reader.parse_network(str);

  const artis::traffic::utils::Network<InputData, OutputData, NodeData, LinkData> &network = reader.network();

  BOOST_CHECK_EQUAL(network.links.at(1).length, 500);
  BOOST_CHECK_EQUAL(network.links.at(1).lane_number, 1);
  BOOST_CHECK_EQUAL(network.nodes.at(1).in_link_number, 0);
  BOOST_CHECK_EQUAL(network.nodes.at(1).out_link_number, 1);
  BOOST_CHECK_EQUAL(network.nodes.at(2).in_link_number, 1);
  BOOST_CHECK_EQUAL(network.nodes.at(2).out_link_number, 0);
}