/**
 * @file tests/vehicle.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/core/Vehicle.hpp>

#define BOOST_TEST_MODULE Core_Vehicle_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

struct Data {
  std::string to_json() const { return "{ }"; }

  std::string to_string() const { return "<>"; }
};

BOOST_AUTO_TEST_CASE(TestCase_Vehicle_1)
{
  artis::traffic::core::Vehicle<Data> v(0, 4.5, 3, 0, 10, 1, 1, 1.2, {0, 0, 1}, Data{});

  BOOST_CHECK_EQUAL(v.current_index(), 0);
  BOOST_CHECK_EQUAL(v.to_string(), "VEHICLE[ 0 ; 4.500000 ; 3.000000 ; 0.000000 / 10.000000 ; 1.000000 / 1.000000 ; 1.200000 ; {<0>, 0, 1} ; <> ]");
  BOOST_CHECK_EQUAL(v.to_json(), "{ \"id\": 0, \"length\": 4.500000, \"gap\": 4.500000, \"speed\": 4.500000, \"max_speed\": 4.500000, \"acceleration\": 4.500000, \"max_acceleration\": 4.500000, \"reaction_time\": 4.500000, \"path\": { \"indexes\": [0, 0, 1], \"current\": 0 }, \"data\": { } }");
}