/**
 * @file tests/path.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2022 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/core/Path.hpp>

#define BOOST_TEST_MODULE Core_Path_Tests

#include <boost/test/unit_test.hpp>

/*************************************************
 * Tests
 *************************************************/

BOOST_AUTO_TEST_CASE(TestCase_Path_1)
{
  std::vector<unsigned int> p{1, 0, 0, 1};
  artis::traffic::core::Path<2, 1> path(p);

  BOOST_CHECK(path.current_index() == 1);
  BOOST_CHECK(path.next_index() == 0);
  path.forward();
  BOOST_CHECK(path.current_index() == 0);
  BOOST_CHECK(path.next_index() == 0);
  path.forward();
  BOOST_CHECK(path.current_index() == 0);
  BOOST_CHECK(path.next_index() == 1);
  path.forward();
  BOOST_CHECK(path.current_index() == 1);
}

BOOST_AUTO_TEST_CASE(TestCase_Path_2)
{
  std::vector<unsigned int> p{0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0};
  artis::traffic::core::Path<6, 3> path(p);

  for (size_t k = 0; k < 3; ++k) {
    for (size_t r = 0; r < 8; ++r) {
      BOOST_CHECK(path.current_index() == r);
      path.forward();
    }
  }
  BOOST_CHECK(path.current_index() == 0);
}

BOOST_AUTO_TEST_CASE(TestCase_Path_3)
{
  std::vector<unsigned int> p{2, 3, 0, 1, 0, 2, 3, 1};
  artis::traffic::core::Path<3, 2> path(p);

  BOOST_CHECK_EQUAL(path.current_index(), 2);
  BOOST_CHECK_EQUAL(path.next_index(), 3);
  BOOST_CHECK_EQUAL(path.to_string(), "{<2>, 3, 0, 1, 0, 2, 3, 1}");
  BOOST_CHECK_EQUAL(path.to_json(), "{ \"indexes\": [2, 3, 0, 1, 0, 2, 3, 1], \"current\": 0 }");
  path.forward();
  BOOST_CHECK_EQUAL(path.to_string(), "{2, <3>, 0, 1, 0, 2, 3, 1}");
  BOOST_CHECK_EQUAL(path.to_json(), "{ \"indexes\": [2, 3, 0, 1, 0, 2, 3, 1], \"current\": 1 }");
}

BOOST_AUTO_TEST_CASE(TestCase_Path_4)
{
  std::vector<unsigned int> p{0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0};
  artis::traffic::core::Path<8, 3> path(p);

  BOOST_CHECK_EQUAL(path.to_string(), "{<0>, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0}");
}