/**
 * @file artis-traffic/utils/JsonReader.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_CORE_JSON_READER_HPP
#define ARTIS_TRAFFIC_CORE_JSON_READER_HPP

#include <nlohmann/json.hpp>

namespace artis::traffic::utils {

enum Level {
  MACRO, MESO, MICRO
};

struct Coordinates {
  double x;
  double y;

  double distance(const Coordinates &other) const {
    return std::sqrt((x - other.x) * (x - other.x) + (y - other.y) * (y - other.y));
  }

  bool is_valid() const { return x != -1 and y != -1; }
};

struct Entity {
  Level level;
  int16_t ID;
};

template<typename Data>
struct Link : Entity {
  int16_t origin_nodeID;
  int16_t destination_nodeID;
  double length;
  double max_speed;
  unsigned int lane_number;
  Data data;
};

template<typename Data>
struct Node : Entity {
  Coordinates coordinates;
  unsigned int in_link_number;
  unsigned int out_link_number;
  Data data;
};

template<typename Data>
struct Input : Entity {
  Coordinates coordinates;
  Data data;
};

template<typename Data>
struct Output : Entity {
  Coordinates coordinates;
  Data data;
};

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
struct Network {
  std::map<int16_t, Input<InputData>> inputs;
  std::map<int16_t, Output<OutputData>> outputs;
  std::map<int16_t, Node<NodeData>> nodes;
  std::map<int16_t, Link<LinkData>> links;
};

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
class JsonReader {
public:
  JsonReader() = default;

  void parse_network(const std::string &str);

  const Network<InputData, OutputData, NodeData, LinkData> &network() const { return _network; }

private:
  void parse_inputs(const nlohmann::json &data, const Level &level);

  void parse_outputs(const nlohmann::json &data, const Level &level);

  void parse_nodes(const nlohmann::json &data, const Level &level);

  void parse_links(const nlohmann::json &data, const Level &level);

  Network<InputData, OutputData, NodeData, LinkData> _network;
};

}

#include <artis-traffic/utils/JsonReader.tpp>

#endif //ARTIS_TRAFFIC_CORE_JSON_READER_HPP
