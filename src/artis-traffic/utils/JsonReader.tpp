/**
 * @file artis-traffic/utils/JsonReader.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/utils/JsonReader.hpp>

using namespace nlohmann;

namespace artis::traffic::utils {

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
void JsonReader<InputData, OutputData, NodeData, LinkData>::parse_network(const std::string &str) {
  json data = json::parse(str);
  std::string level_str = data["level"].get<std::string>();
  Level level{level_str == "macro" ? MACRO : level_str == "meso" ? MESO : MICRO};

  parse_inputs(data, level);
  parse_outputs(data, level);
  parse_nodes(data, level);
  parse_links(data, level);
}

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
void
JsonReader<InputData, OutputData, NodeData, LinkData>::parse_inputs(const nlohmann::json &data, const Level &level) {
  const json &inputs = data["inputs"];

  for (const json &input: inputs) {
    double x = input.contains("x") ? input["x"].get<double>() : -1;
    double y = input.contains("y") ? input["y"].get<double>() : -1;
    Input<InputData> new_input{{level, input["ID"].get<int16_t>()},
                               {x,     y},
                               {}};

    new_input.data.parse(input);
    _network.inputs[new_input.ID] = new_input;
  }
}

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
void
JsonReader<InputData, OutputData, NodeData, LinkData>::parse_outputs(const nlohmann::json &data, const Level &level) {
  const json &outputs = data["outputs"];

  for (const json &output: outputs) {
    double x = output.contains("x") ? output["x"].get<double>() : -1;
    double y = output.contains("y") ? output["y"].get<double>() : -1;
    Output<OutputData> new_output{{level, output["ID"].get<int16_t>()},
                                  {x,     y},
                                  {}};

    new_output.data.parse(output);

    assert(_network.inputs.find(new_output.ID) == _network.inputs.cend());

    _network.outputs[new_output.ID] = new_output;
  }
}

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
void
JsonReader<InputData, OutputData, NodeData, LinkData>::parse_nodes(const nlohmann::json &data, const Level &level) {
  const json &nodes = data["nodes"];

  for (const json &node: nodes) {
    double x = node.contains("x") ? node["x"].get<double>() : -1;
    double y = node.contains("y") ? node["y"].get<double>() : -1;
    Node<NodeData> new_node{{level, node["ID"].get<int16_t>()}, {x, y}, 0, 0, {}};

    new_node.data.parse(node);

    assert(_network.inputs.find(new_node.ID) == _network.inputs.cend() and
           _network.outputs.find(new_node.ID) == _network.outputs.cend());

    _network.nodes[new_node.ID] = new_node;
  }
}

template<typename InputData, typename OutputData, typename NodeData, typename LinkData>
void
JsonReader<InputData, OutputData, NodeData, LinkData>::parse_links(const nlohmann::json &data, const Level &level) {
  const json &links = data["links"];

  for (const json &link: links) {
    int16_t originID = link["origin"].get<int16_t>();
    int16_t destinationID = link["destination"].get<int16_t>();
    double length = 0;
    auto origin_it = _network.nodes.find(originID);
    auto destination_it = _network.nodes.find(destinationID);

    if (origin_it != _network.nodes.end()) {
      origin_it->second.out_link_number++;
    }
    if (destination_it != _network.nodes.end()) {
      destination_it->second.in_link_number++;
    }
    if (not link.contains("length")) {
      if (origin_it != _network.nodes.end() and destination_it != _network.nodes.end() and
          origin_it->second.coordinates.is_valid() and destination_it->second.coordinates.is_valid()) {
        length = origin_it->second.coordinates.distance(destination_it->second.coordinates);
      }
    } else {
      length = link["length"].get<double>();
    }

    unsigned int lane_number = link.contains("lane number") ? link["lane number"].get<unsigned int>() : 1;
    Link<LinkData> new_link{{level, link["ID"].get<int16_t>()}, originID, destinationID, length,
                            link["max speed"].get<double>(), lane_number, {}};

    new_link.data.parse(link);
    _network.links[new_link.ID] = new_link;
  }
}

}