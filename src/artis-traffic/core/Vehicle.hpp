/**
 * @file artis-traffic/core/Vehicle.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_CORE_VEHICLE_HPP
#define ARTIS_TRAFFIC_CORE_VEHICLE_HPP

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/core/Path.hpp>

namespace artis::traffic::core {

template<typename Data>
class Vehicle {
public:
  Vehicle() = default;

  Vehicle(unsigned int index, double length, double gap, double speed, double max_speed, double acceleration,
          double max_acceleration, double reaction_time, const std::vector<unsigned int> &path, const Data &data)
    : _index(index), _length(length), _gap(gap), _speed(speed), _max_speed(max_speed), _acceleration(acceleration),
      _max_acceleration(max_acceleration), _reaction_time(reaction_time), _path(path), _data(data) {
  }

  double acceleration() const { return _acceleration; }

  void backward() {
    _path.backward();
  }

  uint8_t current_index() const {
    return _path.current_index();
  }

  const Data &data() const { return _data; }

  Data &data() { return _data; }

  void forward() {
    _path.forward();
  }

  double gap() const { return _gap; }

  unsigned int index() const { return _index; }

  double length() const { return _length; }

  double max_speed() const { return _max_speed; }

  double max_acceleration() const { return _max_acceleration; }

  uint8_t next_index() const {
    return _path.next_index();
  }

  bool operator==(const Vehicle<Data>& other) const {
    return _index == other._index;
  }

  const Path<8, 3> &path() const { return _path; }

  double reaction_time() const { return _reaction_time; }

  double speed() const { return _speed; }

  std::string to_json() const {
    return "{ \"id\": " + std::to_string(_index) + ", "
           + "\"length\": " + std::to_string(_length) + ", "
           + "\"gap\": " + std::to_string(_length) + ", "
           + "\"speed\": " + std::to_string(_length) + ", "
           + "\"max_speed\": " + std::to_string(_length) + ", "
           + "\"acceleration\": " + std::to_string(_length) + ", "
           + "\"max_acceleration\": " + std::to_string(_length) + ", "
           + "\"reaction_time\": " + std::to_string(_length) + ", "
           + "\"path\": " + _path.to_json() + ", "
           + "\"data\": " + _data.to_json()
           + " }";
  }

  std::string to_string() const {
    return "VEHICLE[ " + std::to_string(_index)
           + " ; " + std::to_string(_length)
           + " ; " + std::to_string(_gap)
           + " ; " + std::to_string(_speed) + " / " + std::to_string(_max_speed)
           + " ; " + std::to_string(_acceleration) + " / " + std::to_string(_max_acceleration)
           + " ; " + std::to_string(_reaction_time)
           + " ; " + _path.to_string()
           + " ; " + _data.to_string()
           + " ]";
  }

  void update_acceleration(double new_acceleration) { _acceleration = new_acceleration; }

  void update_max_speed(double new_max_speed) { _max_speed = new_max_speed; }

  void update_speed(double new_speed) { _speed = new_speed; }

private:
  unsigned int _index;
  double _length;
  double _gap;
  double _speed;
  double _max_speed;
  double _acceleration;
  double _max_acceleration;
  double _reaction_time;
  Path<8, 3> _path;
  Data _data;
};

}

#endif //ARTIS_TRAFFIC_CORE_VEHICLE_HPP