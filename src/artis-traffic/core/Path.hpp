/**
 * @file artis-traffic/core/Path.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_CORE_PATH_HPP
#define ARTIS_TRAFFIC_CORE_PATH_HPP

#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>

namespace artis::traffic::core {

template<size_t LengthBitNumber, size_t ChoiceBitNumber>
class Path {
public:
  Path() = default;

  Path(const std::vector<unsigned int> &path) : _index(0), _length(path.size()) {
    size_t i = 0;

    assert(_length <= (uint16_t) (_max_path_length));

    memset(_indexes, 0, _size * sizeof(uint64_t));
    for (unsigned int p: path) {

      assert(p < _max_choice);

      _indexes[i / _choice_by_cell] |= ((uint64_t) p) << shift(i);
      ++i;
    }
  }

  void backward() {

    assert(_index > 0);

    --_index;
  }

  uint8_t current_index() const {
    return get(_index);
  }

  void forward() {

    assert(_index + 1 < _length);

    ++_index;
  }

  bool has_next() const { return _index < _length - 1; }

  uint8_t next_index() const {

    assert(_index + 1 < _length);

    return get(_index + 1);
  }

  std::string to_json() const {
    std::string str = "{ \"indexes\": [";

    for (uint16_t k = 0; k < _length; ++k) {
      str += std::to_string(get(k));
      if (k < _length - 1) {
        str += ", ";
      }
    }
    str += "], \"current\": " + std::to_string(_index);
    str += " }";
    return str;
  }

  std::string to_string() const {
    std::string str = "{";

    for (uint16_t k = 0; k < _length; ++k) {
      if (k == _index) { str += "<"; }
      str += std::to_string(get(k));
      if (k == _index) { str += ">"; }
      if (k < _length - 1) {
        str += ", ";
      }
    }
    str += "}";
    return str;
  }

private:
  uint8_t get(uint16_t index) const { return (_indexes[index / _choice_by_cell] >> shift(index)) & _mask; }

  size_t shift(uint16_t i) const { return (ChoiceBitNumber * (_choice_by_cell - (i % _choice_by_cell) - 1)); }

  static constexpr size_t _max_path_length = 1 << LengthBitNumber;
  static constexpr size_t _max_choice = 1 << ChoiceBitNumber;
  static constexpr size_t _choice_by_cell = 1 << (7 - ChoiceBitNumber); // 64 = 2^6
  static constexpr size_t _size =
    _max_path_length >> (7 - ChoiceBitNumber) > 1 ? _max_path_length >> (7 - ChoiceBitNumber) : 1;
  static constexpr size_t _mask = ~(0xFF << ChoiceBitNumber);

  uint64_t _indexes[_size];
  uint16_t _index;
  uint16_t _length;
};

}

#endif //ARTIS_TRAFFIC_CORE_PATH_HPP
